using ConsolePaint.Services.Drawing.ExtensionMethods;
using ConsolePaint.Services.Drawing.Models;
using System.IO;
using System.Reflection;
using UnitTests.Helpers;
using Xunit;

namespace UnitTests
{

    public class BucketFillUnitTests
    {

        private string TestPath { get; set; }

        public BucketFillUnitTests()
        {
            TestPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"TestData\");
        }

        [Fact]
        public void TestCanvasWithDiagonalLine()
        {
            Canvas canvas = new Canvas(width: 10, height: 10);
            canvas.DrawLine(1, 1, 10, 10);
            canvas.BucketFill(5, 1, 'c');


            string testDataPath = Path.Combine(TestPath, @"BucketFillUnitTests - TestCanvasWithDiagonalLine.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, canvas.Board);
        }

        [Fact]
        public void TestCanvasWithSquare()
        {
            Canvas canvas = new Canvas(width: 10, height: 10);
            canvas.DrawRectangle(2, 2, 7, 7);
            canvas.BucketFill(5, 5, 'c');

            string testDataPath = Path.Combine(TestPath, @"BucketFillUnitTests - TestCanvasWithSquare.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, canvas.Board);

        }

        [Fact]
        public void TestEmptyCanvas()
        {
            Canvas canvas = new Canvas(width: 10, height: 10);
            canvas.BucketFill(5, 5, 'c');

            string testDataPath = Path.Combine(TestPath, @"BucketFillUnitTests - TestEmptyCanvas.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, canvas.Board);

        }


    }
}
