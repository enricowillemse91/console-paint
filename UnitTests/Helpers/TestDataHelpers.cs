﻿using Newtonsoft.Json;
using System.IO;

namespace UnitTests.Helpers
{
    public static partial class TestDataHelpers
    {
        public static char[,] GetBoard(string filePath)
        {
            string data = File.ReadAllText(filePath);
            char[,] canvas = JsonConvert.DeserializeObject<char[,]>(data);

            return canvas;
        }
    }
}
