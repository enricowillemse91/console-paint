using ConsolePaint.Services.Drawing;
using System.IO;
using System.Reflection;
using UnitTests.Helpers;
using Xunit;

namespace UnitTests
{

    public class DrawingServiceTests
    {

        private string TestPath { get; set; }

        public DrawingServiceTests()
        {
            TestPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"TestData\");
        }

        [Fact]
        public void TestCanvasRender()
        {
            DrawingService service = new DrawingService();

            string command = "R -a 2 -b 2 -c 7 -d 7";
            string[] parsedArgs = command.Split(' ');
            service.ProcessCommands(parsedArgs);

            command = "B -x 5 -y 5 -c c";
            parsedArgs = command.Split(' ');
            service.ProcessCommands(parsedArgs);

            string renderedCanvas = service.RenderCanvas();

            string testDataPath = Path.Combine(TestPath, @"DrawingServiceTests - TestCanvasRender.txt");
            string testData = File.ReadAllText(testDataPath);

            Assert.Equal(testData.Trim(), renderedCanvas.Trim());
        }

        [Fact]
        public void TestDrawingSquareAndFillCommand()
        {
            DrawingService service = new DrawingService();

            string command = "R -a 2 -b 2 -c 7 -d 7";
            string[] parsedArgs = command.Split(' ');
            service.ProcessCommands(parsedArgs);

            command = "B -x 5 -y 5 -c c";
            parsedArgs = command.Split(' ');
            service.ProcessCommands(parsedArgs);


            string testDataPath = Path.Combine(TestPath, @"DrawingServiceTests - TestDrawingSquareAndFillCommand.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, service.Canvas.Board);

        }

    }
}
