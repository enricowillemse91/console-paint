using ConsolePaint.Services.Drawing.ExtensionMethods;
using ConsolePaint.Services.Drawing.Models;
using System.IO;
using System.Reflection;
using UnitTests.Helpers;
using Xunit;

namespace UnitTests
{
    public class DrawLineUnitTests
    {

        private string TestPath { get; set; }

        public DrawLineUnitTests()
        {
            TestPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"TestData\");
        }

        [Fact]
        public void TestDiagonal()
        {
            Canvas canvas = new Canvas(width: 10, height: 10);
            canvas.DrawLine(1, 1, 10, 10);


            string testDataPath = Path.Combine(TestPath, @"DrawLineUnitTests - TestDiagonal.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, canvas.Board);
        }

        [Fact]
        public void TestHorizontal()
        {
            Canvas canvas = new Canvas(width: 10, height: 10);
            canvas.DrawLine(1, 6, 10, 6);

            string testDataPath = Path.Combine(TestPath, @"DrawLineUnitTests - TestHorizontal.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, canvas.Board);
        }

        [Fact]
        public void TestVertical()
        {
            Canvas canvas = new Canvas(width: 10, height: 10);
            canvas.DrawLine(6, 1, 6, 10);

            string testDataPath = Path.Combine(TestPath, @"DrawLineUnitTests - TestVertical.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, canvas.Board);
        }
    }
}
