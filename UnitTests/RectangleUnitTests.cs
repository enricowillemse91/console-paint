using ConsolePaint.Services.Drawing.ExtensionMethods;
using ConsolePaint.Services.Drawing.Models;
using System.IO;
using System.Reflection;
using UnitTests.Helpers;
using Xunit;

namespace UnitTests
{
    public class RectangleUnitTests
    {

        private string TestPath { get; set; }

        public RectangleUnitTests()
        {
            TestPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"TestData\");
        }

        [Fact]
        public void TestDraw()
        {
            Canvas canvas = new Canvas(width: 10, height: 10);
            canvas.DrawRectangle(2, 3, 10, 6);


            string testDataPath = Path.Combine(TestPath, @"RectangleUnitTests - TestDraw.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, canvas.Board);
        }

        [Fact]
        public void TestDrawOutOfBounds()
        {
            Canvas canvas = new Canvas(width: 10, height: 10);
            canvas.DrawRectangle(2, 6, 30, 30);


            string testDataPath = Path.Combine(TestPath, @"RectangleUnitTests - TestDrawOutOfBounds.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, canvas.Board);
        }

        [Fact]
        public void TestDrawZeroSize()
        {
            Canvas canvas = new Canvas(width: 10, height: 10);
            canvas.DrawRectangle(6, 6, 6, 6);


            string testDataPath = Path.Combine(TestPath, @"RectangleUnitTests - TestDrawZeroSize.json");
            char[,] expectedCanvas = TestDataHelpers.GetBoard(testDataPath);

            Assert.Equal(expectedCanvas, canvas.Board);
        }

    }
}
