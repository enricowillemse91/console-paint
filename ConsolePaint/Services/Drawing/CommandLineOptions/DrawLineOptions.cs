// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="DrawLineOptions.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using CommandLine;

namespace ConsolePaint.Services.Drawing.CommandLineOptions
{
    /// <summary>
    /// Class DrawLineOptions.
    /// </summary>
    [Verb("L", HelpText = "Create a new line from (a,b) to (c,d). Draws a line on the canvas using the Bresenham line algorithm")]
    public class DrawLineOptions
    {
        /// <summary>
        /// Gets or sets the x1 coordinate.
        /// </summary>
        /// <value>The x1 coordinate.</value>
        [Option('a', "x1", Required = true, HelpText = "The start x coordinate (a)")]
        public int X1 { get; set; }

        /// <summary>
        /// Gets or sets the y1 coordinate.
        /// </summary>
        /// <value>The y1 coordinate.</value>
        [Option('b', "y1", Required = true, HelpText = "The start y coordinate (b)")]
        public int Y1 { get; set; }

        /// <summary>
        /// Gets or sets the x2 coordinate.
        /// </summary>
        /// <value>The x2 coordinate.</value>
        [Option('c', "x2", Required = true, HelpText = "The end x coordinate (c)")]
        public int X2 { get; set; }

        /// <summary>
        /// Gets or sets the y2 coordinate.
        /// </summary>
        /// <value>The y2 coordinate.</value>
        [Option('d', "y2", Required = true, HelpText = "The end y coordinate (d)")]
        public int Y2 { get; set; }
    }
}
