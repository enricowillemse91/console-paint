// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="BucketFillOptions.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using CommandLine;

namespace ConsolePaint.Services.Drawing.CommandLineOptions
{
    /// <summary>
    /// Class BucketFillOptions.
    /// </summary>
    [Verb("B", HelpText = "Fill the entire area connected to (x,y) with 'color' c. The behaviour of this is the same as that of the 'bucket fill' tool in paint programs.")]
    public class BucketFillOptions
    {
        /// <summary>
        /// Gets or sets the color character.
        /// </summary>
        /// <value>The color character.</value>
        [Option('c', "color", Required = true, HelpText = "The bucket fill color character")]
        public char Color { get; set; }

        /// <summary>
        /// Gets or sets the x coordinate to start filling.
        /// </summary>
        /// <value>The x coordinate.</value>
        [Option('x', "x1", Required = true, HelpText = "The fill x coordinate")]
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the y coordinate to start filling.
        /// </summary>
        /// <value>The y coordinate.</value>
        [Option('y', "y1", Required = true, HelpText = "The fill y coordinate")]
        public int Y { get; set; }
    }
}
