// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="CreateOptions.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using CommandLine;

namespace ConsolePaint.Services.Drawing.CommandLineOptions
{
    /// <summary>
    /// Class CreateOptions.
    /// </summary>
    [Verb("C", HelpText = "Create a new canvas of width w and height h.")]
    public class CreateOptions
    {
        /// <summary>
        /// Gets or sets the name of the new canvas.
        /// </summary>
        /// <value>The canvas name.</value>
        [Option('n', "name", Required = true, HelpText = "Name of the new canvas")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the width of the new canvas.
        /// </summary>
        /// <value>The width of the canvas.</value>
        [Option('w', "width", Required = true, HelpText = "The canvas width")]
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the new canvas.
        /// </summary>
        /// <value>The height of the canvas.</value>
        [Option('h', "height", Required = true, HelpText = "The canvas height")]
        public int Height { get; set; }
    }
}
