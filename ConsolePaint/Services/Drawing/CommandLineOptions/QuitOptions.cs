﻿// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="QuitOptions.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsolePaint.Services.Drawing.CommandLineOptions
{
    /// <summary>
    /// Class QuitOptions.
    /// </summary>
    [Verb("Q", HelpText = "Quit the program.")]
    public class QuitOptions
    {
    }
}
