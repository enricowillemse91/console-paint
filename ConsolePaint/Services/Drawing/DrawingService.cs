﻿// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="DrawingService.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using CommandLine;
using ConsolePaint.Services.Drawing.CommandLineOptions;
using ConsolePaint.Services.Drawing.ExtensionMethods;
using ConsolePaint.Services.Drawing.Models;
using System;
using System.Text;

namespace ConsolePaint.Services.Drawing
{
    /// <summary>
    /// Class DrawingService.
    /// </summary>
    public class DrawingService
    {
        /// <summary>
        /// Gets or sets the canvas.
        /// </summary>
        /// <value>The drawing canvas.</value>
        public Canvas Canvas { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawingService"/> class.
        /// </summary>
        public DrawingService()
        {
            Canvas = new Canvas();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawingService"/> class.
        /// </summary>
        /// <param name="canvasWidth">Width of the canvas.</param>
        /// <param name="canvasHeight">Height of the canvas.</param>
        /// <param name="canvasName">Name of the canvas.</param>
        public DrawingService(int canvasWidth, int canvasHeight, string canvasName)
        {
            Canvas = new Canvas(canvasWidth, canvasHeight, canvasName);
        }

        /// <summary>
        /// Processes the commands.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns><c>true</c> if canvas is updated, <c>false</c> otherwise.</returns>
        public bool ProcessCommands(string[] args)
        {

            ParserResult<object> result = Parser.Default
                .ParseArguments<BucketFillOptions, CreateOptions, DrawLineOptions, DrawRectangleOptions, QuitOptions
                >(args);

            result.WithParsed<QuitOptions>(a => Environment.Exit(0));

            Canvas resCanvas = result.MapResult(
                (BucketFillOptions opts) => Canvas.BucketFill(opts.X, opts.Y, opts.Color),
                (DrawLineOptions opts) => Canvas.DrawLine(opts.X1, opts.Y1, opts.X2, opts.Y2),
                (DrawRectangleOptions opts) => Canvas.DrawRectangle(opts.X1, opts.Y1, opts.X2, opts.Y2),
                (CreateOptions opts) => NewCanvas(opts.Width, opts.Height, opts.Name),
                errs => null);

            return resCanvas != null;
        }

        /// <summary>
        /// Creates new canvas.
        /// </summary>
        /// <param name="canvasWidth">Width of the canvas.</param>
        /// <param name="canvasHeight">Height of the canvas.</param>
        /// <param name="canvasName">Name of the canvas.</param>
        /// <returns>Canvas.</returns>
        public Canvas NewCanvas(int canvasWidth, int canvasHeight, string canvasName)
        {
            Canvas = new Canvas(canvasWidth, canvasHeight, canvasName);
            return Canvas;
        }

        /// <summary>
        /// Renders the canvas for printing on console.
        /// </summary>
        /// <returns>System.String.</returns>
        public string RenderCanvas()
        {
            StringBuilder renderOut = new StringBuilder();
            string topBottomPadding = new string('-', (Canvas.Width + 2) * 3) + Environment.NewLine;

            for (int y = 0; y < Canvas.Height; y++)
            {
                string renderLine = "";
                for (int x = 0; x < Canvas.Width; x++)
                {
                    renderLine += $" {Canvas.Board[x, y]} ";
                }
                renderOut.AppendLine($"|  {renderLine}  |");
            }

            return $"{topBottomPadding}{renderOut}{topBottomPadding}";
        }
    }
}
