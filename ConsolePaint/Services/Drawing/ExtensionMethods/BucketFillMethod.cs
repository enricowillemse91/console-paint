// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="BucketFillMethod.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using ConsolePaint.Services.Drawing.Models;
using System.Collections.Generic;
using System.Drawing;

namespace ConsolePaint.Services.Drawing.ExtensionMethods
{
    /// <summary>
    /// Class ExtensionMethods.
    /// </summary>
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Fill the entire area connected to (x,y) with 'color' c. The behaviour of this is the same as that of the 'bucket fill' tool in paint programs.
        /// </summary>
        /// <param name="canvas">The canvas.</param>
        /// <param name="x">The fill x coordinate.</param>
        /// <param name="y">The fill y coordinate.</param>
        /// <param name="color">The bucket fill color character.</param>
        /// <returns>Canvas.</returns>
        public static Canvas BucketFill(this Canvas canvas, int x, int y, char color)
        {
            Stack<Point> pixels = new Stack<Point>();
            Point pt = new Point(x - 1, y - 1);

            char targetColor = canvas.GetPixel(pt.X, pt.Y);
            pixels.Push(pt);

            while (pixels.Count > 0)
            {
                Point a = pixels.Pop();

                if (a.X >= canvas.Width || a.X < 0 || a.Y >= canvas.Height || a.Y < 0)
                {
                    continue;
                }

                if (canvas.GetPixel(a.X, a.Y) != targetColor)
                {
                    continue;
                }

                canvas.SafeSetPixel(a.X, a.Y, color);
                pixels.Push(new Point(a.X - 1, a.Y));
                pixels.Push(new Point(a.X + 1, a.Y));
                pixels.Push(new Point(a.X, a.Y - 1));
                pixels.Push(new Point(a.X, a.Y + 1));
            }

            return canvas;
        }
    }
}