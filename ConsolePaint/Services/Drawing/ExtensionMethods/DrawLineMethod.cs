// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="DrawLineMethod.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using ConsolePaint.Services.Drawing.Models;
using System;

namespace ConsolePaint.Services.Drawing.ExtensionMethods
{
    /// <summary>
    /// Class ExtensionMethods.
    /// </summary>
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Draws a line on the canvas using the Bresenham line algorithm
        /// </summary>
        /// <param name="canvas">The canvas.</param>
        /// <param name="x1">The x1 coordinate.</param>
        /// <param name="y1">The y1 coordinate.</param>
        /// <param name="x2">The x2 coordinate.</param>
        /// <param name="y2">The y2 coordinate.</param>
        /// <returns>Canvas.</returns>
        public static Canvas DrawLine(this Canvas canvas, int x1, int y1, int x2, int y2)
        {
            int w = x2 - x1;
            int h = y2 - y1;

            int x = x1 - 1;
            int y = y1 - 1;

            int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
            if (w < 0)
            {
                dx1 = -1;
            }
            else if (w > 0)
            {
                dx1 = 1;
            }

            if (h < 0)
            {
                dy1 = -1;
            }
            else if (h > 0)
            {
                dy1 = 1;
            }

            if (w < 0)
            {
                dx2 = -1;
            }
            else if (w > 0)
            {
                dx2 = 1;
            }

            int longest = Math.Abs(w);
            int shortest = Math.Abs(h);
            if (!(longest > shortest))
            {
                longest = Math.Abs(h);
                shortest = Math.Abs(w);
                if (h < 0)
                {
                    dy2 = -1;
                }
                else if (h > 0)
                {
                    dy2 = 1;
                }

                dx2 = 0;
            }
            int numerator = longest >> 1;
            for (int i = 0; i <= longest; i++)
            {
                canvas.SafeSetPixel(x, y, Configs.DrawingService.DefaultValues.LineColor);
                numerator += shortest;
                if (!(numerator < longest))
                {
                    numerator -= longest;
                    x += dx1;
                    y += dy1;
                }
                else
                {
                    x += dx2;
                    y += dy2;
                }
            }
            return canvas;
        }
    }
}