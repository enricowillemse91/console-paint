// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="DrawRectangleMethod.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using ConsolePaint.Services.Drawing.Models;
using System;

namespace ConsolePaint.Services.Drawing.ExtensionMethods
{
    /// <summary>
    /// Class ExtensionMethods.
    /// </summary>
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Create a new rectangle, whose upper left corner is (a,b) and lower right corner is (c,d). Horizontal and vertical lines will be drawn using the 'X' character.
        /// </summary>
        /// <param name="canvas">The canvas.</param>
        /// <param name="x1">The x1 coordinate.</param>
        /// <param name="y1">The y1 coordinate.</param>
        /// <param name="x2">The x2 coordinate.</param>
        /// <param name="y2">The y2 coordinate.</param>
        /// <returns>Canvas.</returns>
        public static Canvas DrawRectangle(this Canvas canvas, int x1, int y1, int x2, int y2)
        {

            int minX = Math.Min(x1, x2) - 1;
            int minY = Math.Min(y1, y2) - 1;

            int maxX = Math.Max(x1, x2) - 1;
            int maxY = Math.Max(y1, y2) - 1;

            char color = Configs.DrawingService.DefaultValues.LineColor;

            for (int i = minX; i <= maxX; i++)
            {
                canvas.SafeSetPixel(i, minY, color);
                canvas.SafeSetPixel(i, maxY, color);
            }

            for (int i = minY; i <= maxY; i++)
            {
                canvas.SafeSetPixel(minX, i, color);
                canvas.SafeSetPixel(maxX, i, color);
            }

            return canvas;
        }
    }
}