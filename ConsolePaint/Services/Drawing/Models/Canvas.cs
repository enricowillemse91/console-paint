﻿// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="Canvas.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace ConsolePaint.Services.Drawing.Models
{
    /// <summary>
    /// Class Canvas.
    /// </summary>
    public class Canvas
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Canvas" /> class.
        /// </summary>
        public Canvas() : this(Configs.DrawingService.DefaultValues.CanvasWidth, Configs.DrawingService.DefaultValues.CanvasHeight)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Canvas" /> class.
        /// </summary>
        /// <param name="width">The new canvas width.</param>
        /// <param name="height">The new canvas height.</param>
        public Canvas(int width, int height) : this(width, height, Configs.DrawingService.DefaultValues.CanvasName)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Canvas" /> class.
        /// </summary>
        /// <param name="width">The new canvas width.</param>
        /// <param name="height">The new canvas width.</param>
        /// <param name="name">The new canvas name.</param>
        public Canvas(int width, int height, string name)
        {
            Board = new char[width, height];
            Name = name;
            Clear();
        }


        /// <summary>
        /// Clears the canvas and fills it with whitespace characters
        /// </summary>
        public void Clear()
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    Board[x, y] = ' ';
                }
            }
        }

        /// <summary>
        /// Gets or sets the canvas name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the drawing board.
        /// </summary>
        /// <value>The canvas board.</value>
        public char[,] Board { get; set; }


        /// <summary>
        /// Gets the canvas width.
        /// </summary>
        /// <value>The canvas width.</value>
        public int Width => Board.GetLength(0);

        /// <summary>
        /// Gets the canvas height.
        /// </summary>
        /// <value>The canvas height.</value>
        public int Height => Board.GetLength(1);


        /// <summary>
        /// Safe set the pixel color character.
        /// </summary>
        /// <param name="x">The x coordinate to update.</param>
        /// <param name="y">The y coordinate to update.</param>
        /// <param name="color">The color to update it to.</param>
        /// <returns><c>true</c> if on board, <c>false</c> otherwise.</returns>
        public bool SafeSetPixel(int x, int y, char color)
        {
            if (x < 0 || y < 0 || x >= Width || y >= Height)
            {
                return false;
            }

            Board[x, y] = color;
            return true;
        }


        /// <summary>
        /// Gets the pixel.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <returns>System.Char.</returns>
        public char GetPixel(int x, int y)
        {
            return Board[x, y];
        }
    }
}