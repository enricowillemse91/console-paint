﻿// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="Program.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using ConsolePaint.Services.Drawing;
using System;

namespace ConsolePaint
{
    /// <summary>
    /// Class Program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void Main(string[] args)
        {
            Console.WriteLine("Welcome to console paint!");
            DrawingService service = new DrawingService();

            while (true)
            {
                Console.WriteLine("Please enter command or type --help for help:");
                string res = Console.ReadLine();

                if (res == null)
                {
                    Console.WriteLine("Empty input :(");
                    continue;
                }

                string[] parsedArgs = res.Split(' ');
                try
                {
                    bool hasResult = service.ProcessCommands(parsedArgs);
                    if (hasResult)
                    {
                        Console.WriteLine(service.RenderCanvas());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

        }
    }
}
