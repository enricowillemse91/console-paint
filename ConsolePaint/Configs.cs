﻿// ***********************************************************************
// Assembly         : ConsolePaint
// Author           : Enrico
// Created          : 11-04-2018
//
// Last Modified By : Enrico
// Last Modified On : 11-04-2018
// ***********************************************************************
// <copyright file="Configs.cs" company="Credit Suisse">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
namespace ConsolePaint
{
    /// <summary>
    /// Class Configs.
    /// </summary>
    public static class Configs
    {
        public static class DrawingService
        {
            public static class DefaultValues
            {
                /// <summary>
                /// The default canvas width
                /// </summary>
                public static int CanvasWidth = 10;
                /// <summary>
                /// The default canvas height
                /// </summary>
                public static int CanvasHeight = 10;
                /// <summary>
                /// The default canvas name
                /// </summary>
                public static string CanvasName = "New Canvas";
                /// <summary>
                /// The default line color
                /// </summary>
                public static char LineColor = 'X';
            }

        }

    }
}
