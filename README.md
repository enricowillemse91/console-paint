﻿# Console Paint

a Simple console version of a drawing program.  At this time, the functionality of the program is quire limited but this might change in the future. 
In a nutshell, the program has the following functionality:
 1. Create a new canvas
 2. Start drawing on the canvas by issuing various commands
 3. Quit


# Quick Start
## Running program

Console paint was developed using a standard console app template in visual studio. The project can be executed by opening the  ConsolePaint.sln file located in the root of the repo with Visual Studio 2017. Project can then be built and executed from there.

### Commands

| Verb | Description | Options |
|--|--|--|
| C | Create a new canvas of width w and height h. | **(-n, --name)** "Name of the new canvas", **(-w, --width)** "The canvas width", **(-h, --height)** "The canvas height" |
| B | Fill the entire area connected to (x,y) with 'color' c. The behaviour of this is the same as that of the 'bucket fill' tool in paint programs. | **(-c, --color)** "The bucket fill color character", **(-x, --x1)** "The fill x coordinate", **(-y, --y1)** "The fill y coordinate" |
| R | Create a new rectangle, whose upper left corner is (a,b) and lower right corner is (c,d). Horizontal and vertical lines will be drawn using the 'X' character. | **(-a, --x1)**, "The upper left corner x coordinate (a)", **(-b, --y1)**,  "The upper left corner y coordinate (b)", **(-c, --x2)**,  "The lower right corner x coordinate (c)", **(-d, --y2)**,  "The lower right corner y coordinate (d)" |
| L | Create a new line from (a,b) to (c,d). Horizontal and vertical lines will be drawn using the 'X' character. | **(-a, --x1)**, "The start x coordinate (a)", **(-b, --y1)**,  "The start y coordinate (b)", **(-c, --x2)**,  "The end x coordinate (c)", **(-d, --y2)**,  "The end y coordinate (d)" |
| Q | Quit the program. |  |

### Examples:
#### Create New Canvas

```
 C -h 13 -w 13 -n NewCanvas
```
```
    ---------------------------------------------
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    ---------------------------------------------
```

#### Draw Rectangle

```
 R --x1 3 --y1 3 --x2 9 --y2 8
```

```
    ---------------------------------------------
    |                                           |
    |                                           |
    |         X  X  X  X  X  X  X               |
    |         X                 X               |
    |         X                 X               |
    |         X                 X               |
    |         X                 X               |
    |         X  X  X  X  X  X  X               |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    ---------------------------------------------
```

#### Draw Line

```
 L --x1 1 --y1 1 --x2 11 --y2 11
```

```
    ---------------------------------------------
    |   X                                       |
    |      X                                    |
    |         X                                 |
    |            X                              |
    |               X                           |
    |                  X                        |
    |                     X                     |
    |                        X                  |
    |                           X               |
    |                              X            |
    |                                 X         |
    |                                           |
    |                                           |
    ---------------------------------------------
```

#### Fill Rectangle

```
 R --x1 3 --y1 3 --x2 9 --y2 8
 B -x 4 -y 4 -c b
```

```
    ---------------------------------------------
    |                                           |
    |                                           |
    |         X  X  X  X  X  X  X               |
    |         X  b  b  b  b  b  X               |
    |         X  b  b  b  b  b  X               |
    |         X  b  b  b  b  b  X               |
    |         X  b  b  b  b  b  X               |
    |         X  X  X  X  X  X  X               |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    |                                           |
    ---------------------------------------------
```

## Running tests

Tests are done using the xUnit Framework and can either be run directly from visual studio or whatever your preferred CI tool is See documentation for xUnit [here](https://xunit.github.io/docs/getting-started-desktop). The simplest is to simply select **Tests > Run > All Tests** from within Visual Studio.
The tests included are as follows and should be able to test both the different painting functions as well as the actual rendering to the command line:

| Test | File | Description |
|--|--|--|
| Drawing Service | DrawingServiceTests.cs |  Testing the parsing and executing command line inputs as a whole. Testing the rendered canvas with expected output|
| Line Drawing | DrawLineUnitTests.cs | Test drawing horizontal, vertical and diagonal lines  |
| Bucket Fill | BucketFillUnitTests.cs | Test filling empty canvas as well as with existing shapes |
| Rectangle Drawing | RectangleUnitTests.cs | Draw standard rectangle on empty canvas, also test drawing rectangle of zero size |


# Functionality

## Bucket Fill
A stack based algorithm was used for our bucket fill method.

 The stack-based 4-way algorithm is as follows:
 - create a stack of type Point to hold various points 
 - set the target color (the one to replace with newer color) to the current point 
 - push the current point position to the stack while the stack is not empty 
 - pop from the stack and set the point to the popped point stay within bounds of the entire canvas 
 - if the pixel we get from the popped point is equal to the target color set the point to the replacement color 
 - push a new point for west, east, south, and north directions (i.e.
   x-1,x+1,y-1,y+1)

## Draw Line

The Bresenham's line algorithm was used to allow for diagonal line drawing besides the simple horizontal and vertical lines.

## Draw Rectangle

No special technique was used to draw rectangles.



